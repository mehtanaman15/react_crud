import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Link, Route, useNavigate, useParams } from 'react-router-dom';

export default function EditUser() {
  useEffect(() => {
    loadUser();
  }, []);
  const history = useNavigate();
  const { id } = useParams();

  const [user, setUser] = useState({
    name: '',
    username: '',
    email: '',
  });
  const loadUser = async () => {
    const result = await axios.get('http://localhost:3003/users/' + id);
    console.log(result.data.name, result.data.username, result.data.email);
    setUser({
      name: result.data.name,
      username: result.data.username,
      email: result.data.email,
    });
  };
  const formChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };
  const handleSubmit = async e => {
    e.preventDefault();
    console.log(user);
    await axios.put('http://localhost:3003/users/' + id, user);
    history('/');
  };
  return (
    <div>
      <form
        style={{ margin: 10 }}
        onSubmit={e => {
          handleSubmit(e);
        }}
      >
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">
            Name
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            value={user.name}
            name="name"
            onChange={e => {
              formChange(e);
            }}
          />
        </div>
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">
            Username
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            value={user.username}
            name="username"
            onChange={e => {
              formChange(e);
            }}
          />
        </div>
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">
            Email address
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            value={user.email}
            name="email"
            onChange={e => {
              formChange(e);
            }}
          />
        </div>
        <Link to="/" className="btn btn-danger" style={{ margin: 10 }}>
          Back
        </Link>
        <button class="btn btn-primary">Edit</button>
      </form>
    </div>
  );
}
