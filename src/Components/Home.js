import React, { useState, useEffect } from 'react';

import axios from 'axios';
import { Link, Route } from 'react-router-dom';

export default function Home() {
  const [users, setUser] = useState([]);
  useEffect(() => {
    loadUsers();
  }, []);

  const loadUsers = async () => {
    const result = await axios.get('http://localhost:3003/users');
    console.log(result.data);
    setUser(result.data);
  };

  return (
    <div className="container">
      <div className="py-4">
        <h1>Users</h1>

        <Link to="/add" className="btn btn-primary" style={{ margin: 10 }}>
          Add User
        </Link>
        <table class="table table-dark table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">User Name</th>
              <th scope="col">Email</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user, index) => (
              <tr>
                <th scope="row">{index + 1}</th>
                <td>{user.name}</td>
                <td>{user.username}</td>
                <td>{user.email}</td>
                <td>
                  <Link
                    style={{ margin: 10 }}
                    className="btn btn-primary mr-2"
                    to={{
                      pathname: `/edit/${user.id}`,
                    }}
                  >
                    Update
                  </Link>
                  <Link
                    style={{ margin: 10 }}
                    className="btn btn-danger"
                    onClick={() => {
                      axios.delete('http://localhost:3003/users/' + user.id);
                      loadUsers();
                    }}
                    to={{
                      pathname: '/',
                    }}
                  >
                    Delete
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
