import axios from 'axios';
import React, { useState } from 'react';
import { Link, Route, useNavigate } from 'react-router-dom';

export default function AddUser() {
  const history = useNavigate();

  const [user, setUser] = useState({
    name: '',
    username: '',
    email: '',
  });
  const formChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };
  const handleSubmit = async e => {
    e.preventDefault();
    console.log(user);
    await axios.post('http://localhost:3003/users', user);
    history('/');
  };
  return (
    <div>
      <form
        style={{ margin: 10 }}
        onSubmit={e => {
          handleSubmit(e);
        }}
      >
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">
            Name
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            value={user.name}
            name="name"
            onChange={e => {
              formChange(e);
            }}
          />
        </div>
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">
            Username
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            value={user.username}
            name="username"
            onChange={e => {
              formChange(e);
            }}
          />
        </div>
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">
            Email address
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            value={user.email}
            name="email"
            onChange={e => {
              formChange(e);
            }}
          />
        </div>
        <Link to="/" className="btn btn-danger" style={{ margin: 10 }}>
          Back
        </Link>
        <button class="btn btn-primary">Submit</button>
      </form>
    </div>
  );
}
