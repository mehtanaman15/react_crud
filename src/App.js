import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import Home from './Components/Home';
import AddUser from './Components/AddUser';
import EditUser from './Components/EditUser';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Home></Home>}></Route>
          <Route path="/add" element={<AddUser></AddUser>}></Route>
          <Route path="/edit/:id" element={<EditUser></EditUser>}></Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
